﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class MainForm : Form
    {
        Tree Tree = new Tree();
        public MainForm()
        {
            InitializeComponent();
        }

        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }

        private void count_Click(object sender, EventArgs e)
        {
            OutWordForm readForm = new OutWordForm(FormState.SEARCH);
            readForm.ShowDialog();
            if (readForm.Ok)
            {
                MessageBox.Show("Count word with letter '" + readForm.C + "' = " + Convert.ToString(Tree.CountWord(readForm.C)));
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            OutWordForm readForm = new OutWordForm(FormState.ADD);
            readForm.ShowDialog();
            if (readForm.Ok)
            {
                Tree.Add(readForm.txt);
                Redraw();
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            OutWordForm readForm = new OutWordForm(FormState.DELETE);
            readForm.ShowDialog();
            if (readForm.Ok)
            {
                if (Tree.Delete(readForm.txt))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Not found");
                }
            }
        }
    }
}
