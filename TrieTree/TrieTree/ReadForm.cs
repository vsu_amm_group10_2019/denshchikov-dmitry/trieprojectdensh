﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class OutWordForm : Form
    {
        FormState FormState;
        public string txt;
        public bool Ok {get; set;} = false;
        public char C { get; set; }
        public OutWordForm(FormState formState)
        {
            InitializeComponent();
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        text.Text = "Read word for add";
                        break;
                    }
                case FormState.DELETE:
                    {
                        text.Text = "Read word for delete";
                        break;
                    }
                case FormState.SEARCH:
                    {
                        text.Text = "Read letter for count";
                        break;
                    }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.ADD)
            {
                txt = textBox.Text;
                Ok = true;
                Close();
            }
            else
                if (FormState == FormState.DELETE)
                {
                    txt = textBox.Text;
                    Ok = true;
                    Close();
                }
                else
                    if (FormState == FormState.SEARCH)
                    {
                        C = Convert.ToChar(textBox.Text);
                        Ok = true;
                        Close();
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
        }
    }
}
